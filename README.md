### exercism prep tests
> Utility to prep/enable ignored tests for [Exercism](http://www.exercism.org) exercises.

##### How to use

> You will need to install the [Rust compiler](http://www.rust-lang.org) and cargo package tool
to install from Crates.io or build from repository source.

* On [Void Linux](https://voidlinux.org), you can install the following packages with [XBPS](https://github.com/void-linux/xbps):

<pre># xbps-install -S rustc cargo</pre>

* From this repo, copy .languages file (*Unix-likes such as Linux, Mac OS X*) or languages.cfg (*Windows*) to the exercism CLI's working directory (usually ~/exercism on *Unix-likes*). This file contains the definitions for test files in various programming languages and is extendable by the user.

* Install exercism prep tests tool from Crates.io:

<pre>$ cargo install exercism_prep_tests</pre> OR
<pre>$ cargo install --path .</pre> (from cloned repo under exercism prep tests directory).

Run <pre>$ exercism\_prep\_tests</pre> under the exercise directory (e.g. ~/exercism/cpp/hamming) to automatically enable all tests (if the chosen language has been defined).
