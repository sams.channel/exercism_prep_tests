/*
 * Utility to prep/enable ignored tests for Exercism exercises.
 * Author: Sam St-Pettersen, 2023.
*/

use clioptions::CliOptions;
use glob::glob;
use std::io::{BufRead, BufReader, Write};
use std::fs::File;
use std::path::Path;
use std::process::exit;

fn start(program: &str, verbose: bool, quiet: bool) {
    if cfg!(windows) {
        let user = envmnt::get_or_panic("username");
        let defs = &format!("C:\\Users\\{}\\Exercism\\languages.cfg", user);
        if !Path::new(defs).exists() {
            display_error(&program,
            &format!("Could not find C:\\Users\\{}\\Exercism\\languages.cfg file", user));
        }

        cycle_tests(program, defs, verbose, quiet);
    }
    else {
        let user = envmnt::get_or_panic("USER");
        let defs = &format!("/home/{}/exercism/.languages", user);
        if !Path::new(defs).exists() {
            display_error(&program, "Could not find ~/exercism/.languages file");
        }

        cycle_tests(program, defs, verbose, quiet);
    }
}

fn cycle_tests(program: &str, defs: &str, verbose: bool, quiet: bool) {
    let f = File::open(defs).unwrap();
    let reader = BufReader::new(f);

    for r in reader.lines() {
        let l = r.unwrap();
        let split = l.split(" ");
        let path_pattn_repl_insert: Vec<&str> = split.collect();
        prep_tests(program, path_pattn_repl_insert, verbose, quiet);
    }
}

fn prep_tests(program: &str, path_pattn_repl_insert: Vec<&str>, verbose: bool, quiet: bool) {
    for entry in glob(path_pattn_repl_insert[0]).expect("Failed to read glob pattern") {
        match entry {
            Ok(path) => {
                if !quiet {
                    println!("Found tests: {:?}", path.display());
                }
                if !Path::new(&path).exists() {
                    display_error(&program, "Could not find file");
                }

                let mut displayed: bool = false;
                let mut across_lines: bool = false;
                let mut tests: u32 = 0;
                let mut lines: u32 = 0;
                let mut out: Vec<String> = Vec::new();
                let f = File::open(&path).unwrap();
                let reader = BufReader::new(f);

                for r in reader.lines() {
                    let mut l = r.unwrap();
                    if path_pattn_repl_insert.len() == 2 &&
                    l.contains(path_pattn_repl_insert[1]) {
                        tests += 1;
                        if verbose && !displayed {
                            println!("Skipped line(s): '{}'", path_pattn_repl_insert[1]);
                            displayed = true;
                        }
                        continue; // Skip lines containing ignore pattern
                                  // to enable those tests.
                    }
                    else if path_pattn_repl_insert.len() == 3 {
                        // Do a pattern replace.
                        let mut original = path_pattn_repl_insert[1].to_string();
                        let mut replacement = path_pattn_repl_insert[2].to_string();
                        original = original.replace("-", " ");
                        replacement = replacement.replace("-", " ");
                        if verbose && !displayed {
                            println!("Replaced: '{}'", original);
                            println!("With: '{}'", replacement);
                            displayed = true;
                        }
                        across_lines = true;
                        l = l.replace(&original, &replacement);
                    }

                    lines += 1;
                    out.push(l);
                }

                if path_pattn_repl_insert.len() == 4 {
                    // Insert text at beginning of file.
                    if verbose {
                        println!("Inserted line at top: '{}'", path_pattn_repl_insert[3]);
                    }
                    across_lines = true;
                    let mut insert = path_pattn_repl_insert[3].to_string();
                    insert = insert.replace("-", " ");
                    out.insert(0, insert);
                    out.insert(1, "".to_string());
                }

                let mut f = File::create(&path).unwrap();
                for o in &out {
                    writeln!(&mut f, "{}", o).unwrap();
                }

                if across_lines && !quiet {
                    println!("Enabled test(s) across {} line(s).", lines);
                }
                else if !quiet {
                    println!("Enabled {} test(s).", tests);
                }

            },
            Err(e) => display_error(&program, &format!("{:?}", e)),
        }
    }
}

fn display_version(program: &str) {
    println!("{} v0.6.0", program);
    exit(0);
}

fn display_error(program: &str, message: &str) {
    println!("Error: {}.\n", message);
    display_usage(program, -1);
}

fn display_usage(program: &str, code: i32) {
    println!("Utility to prep/enable ignored tests for Exercism exercises.");
    println!("Written by Sam St-Pettersen.\n");
    println!("Usage: {} in relevant exercism exercise directory.\n", program);
    println!("Options:");
    println!("-h | --help: Display this help/usage information and exit.");
    println!("-v | --version: Display version information and exit.");
    println!("-b | --verbose: Be verbose for operations.");
    println!("-q | --quiet: Don't output anything for operations; except errors.");
    exit(code);
}

fn main() {
    let cli = CliOptions::new("exercism_prep_tests");
    let program = cli.get_program();

    let mut verbose: bool = false;
    let mut quiet: bool = false;

    if cli.get_num() > 1 {
        for a in cli.get_args().iter() {
            match a.trim() {
                "-h" | "--help" => display_usage(&program, 0),
                "-v" | "--version" => display_version(&program),
                "-b" | "--verbose" => verbose = true,
                "-q" | "--quiet" => quiet = true,
                _ => {},
            }
        }
    }

    start(&program, verbose, quiet);
    exit(0);
}
